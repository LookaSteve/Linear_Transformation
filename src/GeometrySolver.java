import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Jama.Matrix;

import javax.swing.JSplitPane;
import java.awt.GridLayout;
import javax.swing.JTextField;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;

public class GeometrySolver extends JFrame {

	private JPanel contentPane;
	private JTextArea txtrWelcomeToThis;
	private JPanel panel_1;
	private JButton btnNewButton;
	private JPanel panel_2;
	private JCheckBox chckbxForceRounding;
	private JTextField textField;
	private JComboBox comboBox;
	private JLabel lblMatrixDimensions;
	private JPanel panel_3;
	private JPanel panel_4;
	private JMenuBar menuBar;
	private JMenu mnFile;
	private JMenuItem mntmOpenMatrix;
	private JMenuItem mntmSaveMatrix;
	private JMenuItem mntmSaveMatrixAs;
	private JMenuItem mntmExit;
	private JMenu mnEdit;
	private JMenu mnMatrix;
	private JMenu mnHelp;
	private JMenuItem mntmSolve;
	private JMenuItem mntmAbout;
	private JMenu mnMatrixDimensions;
	private JRadioButtonMenuItem rdbtnmntmX;
	private JRadioButtonMenuItem rdbtnmntmX_1;
	private JRadioButtonMenuItem rdbtnmntmX_2;
	private JRadioButtonMenuItem rdbtnmntmX_3;
	private JRadioButtonMenuItem rdbtnmntmX_4;
	private JMenuItem mntmClear;
	private JMenuItem mntmClearConsole;
	
	private int matrixWidth = 3;
	private List<JTextField> listOfFields = new ArrayList<JTextField>();
	private JPanel panel;
	private GeometrySolver thisObj;
	private JButton btnClearMatrix;
	private int backRound = 0;
	private JComboBox comboBox_1;
	private JLabel lblRotationAxis;
	private Matrix axisMatrix = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GeometrySolver frame = new GeometrySolver();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GeometrySolver() {
		setTitle("Linear Transformation Characterizer");
		
		thisObj = this;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 661, 349);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		mntmOpenMatrix = new JMenuItem("Open matrix");
		mnFile.add(mntmOpenMatrix);
		
		mntmSaveMatrix = new JMenuItem("Save matrix");
		mnFile.add(mntmSaveMatrix);
		
		mntmSaveMatrixAs = new JMenuItem("Save matrix as");
		mnFile.add(mntmSaveMatrixAs);
		
		mntmExit = new JMenuItem("Exit");
		mnFile.add(mntmExit);
		
		mnEdit = new JMenu("Edit");
		menuBar.add(mnEdit);
		
		mnMatrixDimensions = new JMenu("Matrix dimensions");
		mnEdit.add(mnMatrixDimensions);
		
		rdbtnmntmX = new JRadioButtonMenuItem("2 x 2");
		rdbtnmntmX.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				switchDimension(0);
				
			}
		});
		mnMatrixDimensions.add(rdbtnmntmX);
		
		rdbtnmntmX_1 = new JRadioButtonMenuItem("3 x 3");
		rdbtnmntmX_1.setSelected(true);
		rdbtnmntmX_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				switchDimension(1);
				
			}
		});
		mnMatrixDimensions.add(rdbtnmntmX_1);
		
		rdbtnmntmX_2 = new JRadioButtonMenuItem("4 x 4");
		rdbtnmntmX_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				switchDimension(2);
				
			}
		});
		mnMatrixDimensions.add(rdbtnmntmX_2);
		
		rdbtnmntmX_3 = new JRadioButtonMenuItem("5 x 5");
		rdbtnmntmX_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				switchDimension(3);
				
			}
		});
		mnMatrixDimensions.add(rdbtnmntmX_3);
		
		rdbtnmntmX_4 = new JRadioButtonMenuItem("6 x 6");
		rdbtnmntmX_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				switchDimension(4);
				
			}
		});
		mnMatrixDimensions.add(rdbtnmntmX_4);
		
		mnMatrix = new JMenu("Matrix");
		menuBar.add(mnMatrix);
		
		mntmSolve = new JMenuItem("Solve");
		mntmSolve.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				solve();
				
			}
		});
		mnMatrix.add(mntmSolve);
		
		mntmClear = new JMenuItem("Clear");
		mntmClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				clearFields();
				
			}
		});
		mnMatrix.add(mntmClear);
		
		mntmClearConsole = new JMenuItem("Clear console");
		mntmClearConsole.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				txtrWelcomeToThis.setText("");
				
			}
		});
		mnMatrix.add(mntmClearConsole);
		
		mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		mntmAbout = new JMenuItem("About");
		mnHelp.add(mntmAbout);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.4);
		contentPane.add(splitPane, BorderLayout.CENTER);
		
		panel = new JPanel();
		splitPane.setLeftComponent(panel);
		panel.setLayout(new GridLayout(0, matrixWidth, 0, 0));
		
		loadFields();
		
		JScrollPane scrollPane = new JScrollPane();
		splitPane.setRightComponent(scrollPane);
		
		txtrWelcomeToThis = new JTextArea();
		txtrWelcomeToThis.setText("Welcome to this linear transformation characterizer\r\n\r\nFirst enter your matrix on the fields on the left and then hit solve.\r\n\r\nMade by Josselin S.");
		txtrWelcomeToThis.setEditable(false);
		scrollPane.setViewportView(txtrWelcomeToThis);
		
		panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.SOUTH);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JButton btnSolve = new JButton("Solve");
		panel_1.add(btnSolve, BorderLayout.CENTER);
		
		btnNewButton = new JButton("Clear console");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				txtrWelcomeToThis.setText("");
				
			}
		});
		panel_1.add(btnNewButton, BorderLayout.EAST);
		
		btnClearMatrix = new JButton("Clear matrix");
		btnClearMatrix.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				clearFields();
				
			}
		});
		panel_1.add(btnClearMatrix, BorderLayout.WEST);
		
		panel_2 = new JPanel();
		contentPane.add(panel_2, BorderLayout.NORTH);
		ActionListener actionListener = new ActionListener() {
		      public void actionPerformed(ActionEvent actionEvent) {
		        AbstractButton abstractButton = (AbstractButton) actionEvent.getSource();
		        if(abstractButton.getModel().isSelected()){
		        	textField.setEnabled(true);
		        	textField.setText(backRound+"");
		        }else{
		        	backRound = Integer.parseInt(textField.getText());
		        	textField.setText("automatic");
		        	textField.setEnabled(false);
		        }
		      }
		    };
		
		String[] matrices = { "2 x 2", "3 x 3", "4 x 4", "5 x 5", "6 x 6" };
		String[] axises = { "Auto","X", "Y", "Z", "Custom"};
		panel_2.setLayout(new BorderLayout(0, 0));
		
		panel_3 = new JPanel();
		panel_2.add(panel_3, BorderLayout.WEST);
		
		lblMatrixDimensions = new JLabel("Matrix dimensions");
		panel_3.add(lblMatrixDimensions);
		
		comboBox = new JComboBox(matrices);
		ActionListener cbActionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            	switchDimension(comboBox.getSelectedIndex());

            }
        };
        ActionListener cbAxisActionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            	if(comboBox_1.getSelectedIndex() == 4){
            		
            		AxisEditor ae = new AxisEditor(thisObj);
            		
            		
            		ae.setVisible(true);
            	}

            }
        };

        comboBox.addActionListener(cbActionListener);
		panel_3.add(comboBox);
		comboBox.setSelectedIndex(1);
		
		lblRotationAxis = new JLabel("Rotation axis");
		panel_3.add(lblRotationAxis);
		
		comboBox_1 = new JComboBox(axises);
		comboBox_1.addActionListener(cbAxisActionListener);
		panel_3.add(comboBox_1);
		
		panel_4 = new JPanel();
		panel_2.add(panel_4, BorderLayout.EAST);
		
		chckbxForceRounding = new JCheckBox("Force rounding");
		panel_4.add(chckbxForceRounding);
		
		textField = new JTextField();
		panel_4.add(textField);
		textField.setEnabled(false);
		textField.setText("automatic");
		textField.setColumns(10);
		chckbxForceRounding.addActionListener(actionListener);
		btnSolve.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				solve();
				
			}
		});
	}
	
	public void solve(){
		
		boolean isProjection = false;
		boolean isSymmetry = false;
		boolean isRotation = false;
		
		txtrWelcomeToThis.setText("");
		
		log("Parsing...");
		log("");
		
		for(int i = 0; i < matrixWidth * matrixWidth; i ++){
			
			listOfFields.get(i).setText(cleanStr(listOfFields.get(i).getText()));
			
		}
		
		double[] fieldValues = new double[matrixWidth * matrixWidth] ;
		
		try{
			
			for(int i = 0; i < matrixWidth * matrixWidth; i ++){
				
				fieldValues[i] = Double.parseDouble(listOfFields.get(i).getText());
				
			}
			
		}catch(NumberFormatException e){
			
			log("");
			log("The matrix is malformed, ERROR.");
			
			return;
			
		}
		
		double[][] matrixArray = new double[matrixWidth][matrixWidth] ;
		
		for(int i = 0; i < matrixWidth ; i ++){
			
			for(int j = 0; j < matrixWidth ; j ++){
				
				matrixArray[i][j]=fieldValues[(i * matrixWidth)+j];
				
			}
			
		}
		
		Matrix M = new Matrix(matrixArray);
		Matrix ID = Matrix.identity(matrixWidth, matrixWidth);

		Matrix MSQRD = M.times(M);
		Matrix TMO = M.transpose().times(M);
		
		log("M : ");
		log(matrixArrayToString(M.getArray()));
		log("M� : ");
		log(matrixArrayToString(MSQRD.getArray()));
		log("tM * M : ");
		log(matrixArrayToString(TMO.getArray()));
		
		int rounding = 0;
		if(chckbxForceRounding.isSelected()){
			log("Rounding : " + "[tM * M]");
			rounding = Integer.parseInt(cleanStr(textField.getText()));
			log("Forced rounding value of " + rounding + " for " + "[tM * M]");
			log("");
		}else{
			rounding = findIdealRounding(M,"[tM * M]");
		}
		
		TMO = roundMatVir(TMO, rounding);
		
		log("tM * M (Rounded) : ");
		log(matrixArrayToString(TMO.getArray()));
		
		if(chckbxForceRounding.isSelected()){
			log("Rounding : " + "[M�]");
			rounding = Integer.parseInt(cleanStr(textField.getText()));
			log("Forced rounding value of " + rounding + " for " + "[M�]");
			log("");
		}else{
			rounding = findIdealRounding(M,"[M�]");
		}
		
		MSQRD = roundMatVir(MSQRD, rounding);
		
		log("M� (Rounded) : ");
		log(matrixArrayToString(MSQRD.getArray()));
		
		log("Determinant : " + Math.round(M.det()));
		log("");
		
		log("Finding the right definition : ");
		log("");
		
		if(equalMat(MSQRD,M)){
			
			log("Projection, because M� = M");
			
			isProjection = true;
			
		}else if(equalMat(MSQRD,ID)){
			
			log("Symmetry, because M� = Id");
			
			isSymmetry = true;
			
			if(equalMat(TMO,ID)){
				
				log("Isometry, because tM * M = Id");
				
				if((int) Math.round(M.det()) == 1){
					
					log("Axial Orthogonal Symmetry, because det(M) = 1");
					
				}
				
				if((int) Math.round(M.det()) == -1){
					
					log("Plane Orthogonal Symmetry, because det(M) = -1");
					
				}
				
			}else{
				
				log("Not isometric nor plane symmetric, because tM * M != Id");
				
			}
			
		}else{
			
			if(equalMat(TMO,ID)){
				
				log("Rotation, because M is not a symmetry nor a projection but has tM * M = Id");
				
				isRotation = true;
				
			}else{
				
				log("Unknown");
				log("");
				log("If you think the matrix should not be characterized as unknown please try to manually set the rounding amount.");
				
			}
			
		}
		
		//In all cases
		solveKernel(M,ID);
		
		if(isSymmetry){
			
			//Only for Symmetry
			solveMPlusId(M,ID);
			
		}
		
		if(isProjection){
			
			log("");
			log("To solve M Kernel consider the Kernel as the following equation : ");
			log("L1 : x + y + z = 0");
			log("L1 : x + y + z = 0");
			log("L1 : x + y + z = 0");
			log("");
			log("Now if all the equations ar the same the Kernel of M is a plane");
			log("The kernel might also be a line if the equations can be expressed as x = y");
			
		}
		
		if(isRotation){
			
			log("Trying to solve the angle...");
			log("");
			log("/!\\ Warning the angle calculator is in beta, results may be incorrect");
			
			
			double[][] doubleArrayXmat = new double[3][1];
			
			if(comboBox_1.getSelectedIndex() == 0){
				
				log("");
				log("Finding the right axis of rotation...");
				
				findIdealAxis(doubleArrayXmat,M);
				
			}
			if(comboBox_1.getSelectedIndex() == 1){
				doubleArrayXmat[0][0]=0;
				doubleArrayXmat[1][0]=0;
				doubleArrayXmat[2][0]=1;
			}
			if(comboBox_1.getSelectedIndex() == 2){
				doubleArrayXmat[0][0]=1;
				doubleArrayXmat[1][0]=0;
				doubleArrayXmat[2][0]=0;
			}
			if(comboBox_1.getSelectedIndex() == 3){
				doubleArrayXmat[0][0]=0;
				doubleArrayXmat[1][0]=1;
				doubleArrayXmat[2][0]=0;
			}
			if(comboBox_1.getSelectedIndex() == 4 && axisMatrix != null){
				doubleArrayXmat = axisMatrix.getArray();
			}
			
			
			Matrix XMat = new Matrix(doubleArrayXmat);
			
			log("");
			log("X matrix defined as :");
			
			log(matrixArrayToString(XMat.getArray()));
			
			Matrix XxM = M.times(XMat);
			
			log("X * M:");
			
			log(matrixArrayToString(XxM.getArray()));
			
			log("Scalar product :");
			
			Matrix prodScalaire = XMat.transpose().times(XxM);
			
			log(matrixArrayToString(prodScalaire.getArray()));
			
			double a = XMat.getArray()[0][0];
			double b = XMat.getArray()[1][0];
			double c = XMat.getArray()[2][0];
			double norm = Math.sqrt((a*a)+(b*b)+(c*c))*Math.sqrt((a*a)+(b*b)+(c*c));
			
			log("Norm : " + norm);
			log("");
			
			double radAngle = Math.acos(prodScalaire.getArray()[0][0] / norm);
			
			log("Angle in radians : " + radAngle);
			log("");
			log("Angle in degrees : " + Math.toDegrees(radAngle));
			log("");
			log("Angle in degrees (Rounded) : " + round(Math.toDegrees(radAngle),rounding));
			
		}
		
	}
	
	public void findIdealAxis(double[][] doubleArrayXmat,Matrix M){
		
		double[][] doubleArrayTestX = new double[3][1];
		double[][] doubleArrayTestY = new double[3][1];
		double[][] doubleArrayTestZ = new double[3][1];

		doubleArrayTestX[0][0]=0;
		doubleArrayTestX[1][0]=0;
		doubleArrayTestX[2][0]=1;

		doubleArrayTestY[0][0]=1;
		doubleArrayTestY[1][0]=0;
		doubleArrayTestY[2][0]=0;

		doubleArrayTestZ[0][0]=0;
		doubleArrayTestZ[1][0]=1;
		doubleArrayTestZ[2][0]=0;
			
		Matrix XMatTestX = new Matrix(doubleArrayTestX);
		Matrix XMatTestY = new Matrix(doubleArrayTestY);
		Matrix XMatTestZ = new Matrix(doubleArrayTestZ);
		
		Matrix prodScalaireTestX = XMatTestX.transpose().times(M.times(XMatTestX));
		Matrix prodScalaireTestY = XMatTestY.transpose().times(M.times(XMatTestY));
		Matrix prodScalaireTestZ = XMatTestZ.transpose().times(M.times(XMatTestZ));
		
		double comparaisonAngleZero = 1;
		
		double resTestX = prodScalaireTestX.getArray()[0][0];
		double resTestY = prodScalaireTestY.getArray()[0][0];
		double resTestZ = prodScalaireTestZ.getArray()[0][0];
		
		System.out.println(matrixArrayToString(prodScalaireTestX.getArray()));
		System.out.println(matrixArrayToString(prodScalaireTestY.getArray()));
		System.out.println(matrixArrayToString(prodScalaireTestZ.getArray()));

		
		if(resTestY == comparaisonAngleZero){
			
			doubleArrayXmat[0][0]=0;
			doubleArrayXmat[1][0]=0;
			doubleArrayXmat[2][0]=1;
			
			log("X axis rotation detected");
			
		}else if(resTestZ == comparaisonAngleZero){
			
			doubleArrayXmat[0][0]=1;
			doubleArrayXmat[1][0]=0;
			doubleArrayXmat[2][0]=0;
			
			log("Y axis rotation detected");
			
		}else if(resTestX == comparaisonAngleZero){
			
			doubleArrayXmat[0][0]=0;
			doubleArrayXmat[1][0]=1;
			doubleArrayXmat[2][0]=0;
			
			log("Z axis rotation detected");
			
		}else{
			
			log("Could not find the best axis, please try to manually set the axis.");
			
		}
			
	}
	
	public void switchDimension(int value){
		
		removeAllFields();
    	
    	matrixWidth = value+2;
    	
    	panel.setLayout(new GridLayout(0, matrixWidth, 0, 0));
    	
    	loadFields();
    	
    	int extendedState = thisObj.getExtendedState();
    	thisObj.pack();
    	thisObj.setExtendedState(extendedState);
    	
    	rdbtnmntmX.setSelected(false);
    	rdbtnmntmX_1.setSelected(false);
    	rdbtnmntmX_2.setSelected(false);
    	rdbtnmntmX_3.setSelected(false);
    	rdbtnmntmX_4.setSelected(false);
    	
    	switch (value) {
			case 0:rdbtnmntmX.setSelected(true);break;
			case 1:rdbtnmntmX_1.setSelected(true);break;
			case 2:rdbtnmntmX_2.setSelected(true);break;
			case 3:rdbtnmntmX_3.setSelected(true);break;
			case 4:rdbtnmntmX_4.setSelected(true);break;
		}
    	
    	comboBox.setSelectedIndex(value);
		
	}
	
	public void clearFields(){
		
		for(int i = 0; i < matrixWidth * matrixWidth; i ++){
			
			listOfFields.get(i).setText("");
			
		}
		
	}
	
	public void removeAllFields(){
		
		for(int i = 0; i < matrixWidth * matrixWidth; i ++){
			
			panel.remove(listOfFields.get(i));  
			
		}
		
		listOfFields.clear();
		
	}
	
	public void loadFields(){
		
		for(int i = 0; i < matrixWidth * matrixWidth; i ++){
			
			JTextField matF = new JTextField();
			matF.setHorizontalAlignment(SwingConstants.CENTER);
			matF.setFont(new Font("Tahoma", Font.PLAIN, 40));
			panel.add(matF);
			matF.setColumns(10);
			
			listOfFields.add(matF);
			
		}
		
	}
	
	public int findIdealRounding(Matrix M, String name){
		
		log("Rounding : " + name);
		
		int returnValue = 0;
		
		double[][] arrayA = M.getArray();
		
		for(int i = 0; i < matrixWidth; i ++){
			
			for(int j = 0; j < matrixWidth; j++){
				
				String strDigit = new String(arrayA[i][j]+"");
				
				if(strDigit.contains(".") && !strDigit.endsWith(".0")){
					
					String[] splittedStr = strDigit.split("\\.");
					
					if(splittedStr.length > 1){
						
						if(splittedStr[1].length() > returnValue){
							
							returnValue = splittedStr[1].length();
							
						}
						
					}
					
				}
				
			}
			
		}
		
		log("Found ideal rounding value of " + returnValue + " for " + name);
		log("");
		
		return returnValue;
		
	}
	
	public void solveKernel(Matrix M, Matrix Id){
		
		log("");
		log("Solving the kernel...");
		
		Matrix KER = M.minus(Id);
		
		log("");
		log("KER = M - Id :");
		
		log(matrixArrayToString(KER.getArray()));
		
	}
	
	public void solveMPlusId(Matrix M, Matrix Id){
		
		log("This matrix represents the matrix's equation");
		log("");
		log("Solving M + Id kernel...");
		
		Matrix KER = M.plus(Id);
		
		log("");
		log("KER (M + Id) = M + Id :");
		
		log(matrixArrayToString(KER.getArray()));
		log("This matrix represents the symetry's equation");
		
	}
	
	
	public boolean equalMat(Matrix A, Matrix B){
		
		boolean isEqual = true;
		
		double[][] arrayA = A.getArray();
		double[][] arrayB = B.getArray();
		
		for(int i = 0; i < matrixWidth; i ++){
			
			for(int j = 0; j < matrixWidth; j++){
				
				if(arrayA[i][j] != arrayB[i][j]){
					
					isEqual = false;
					
				}
				
			}
			
		}
		
		return isEqual;
		
	}
	
	public Matrix roundMat(Matrix M){
		
		double[][] array = M.getArray();
		
		for(int i = 0; i < matrixWidth; i ++){
			
			for(int j = 0; j < matrixWidth; j++){
				
				array[i][j] = (int) Math.round(array[i][j]);
				
			}
			
		}

		return new Matrix(array);
		
	}
	
	public Matrix roundMatVir(Matrix M,int vir){
		
		double[][] array = M.getArray();
		
		for(int i = 0; i < matrixWidth; i ++){
			
			for(int j = 0; j < matrixWidth; j++){
				
				array[i][j] = round(array[i][j],vir);
				
			}
			
		}

		return new Matrix(array);
		
	}
	
	public double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
	public String cleanStr(String s){
		
		if(s.equals("")){
			
			s = "0";
			
		}
		
		s = s.replace(",", ".");
		
		s = s.replace(" ", "");
		
		return s;
		
	}
	
	public String matrixArrayToString(double[][] array){
		
		String returnString = "";
		
		for(int i = 0; i < array.length; i ++){
			
			for(int j = 0; j < array[0].length; j++){
				
				returnString += " " + array[i][j];
				
			}
			
			returnString += " " + getCR();
			
		}
		
		return returnString;
		
	}
	
	public void log(String s){
		
		txtrWelcomeToThis.append(s + getCR());
		
	}
	
	public String getCR(){
		return System.getProperty("line.separator");
	}

	public Matrix getAxisMatrix() {
		return axisMatrix;
	}

	public void setAxisMatrix(Matrix axisMatrix) {
		this.axisMatrix = axisMatrix;
	}

	
	
}
